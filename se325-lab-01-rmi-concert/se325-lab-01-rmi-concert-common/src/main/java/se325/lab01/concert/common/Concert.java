package se325.lab01.concert.common;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Common Concert implementation
 */
public class Concert implements Serializable{

    private static final long serialVersionUID = 1L;

    private Long id;
    private String title;
    private LocalDateTime date;

    public Concert(String title, LocalDateTime date){
        this.title = title;
        this.date = date;
    }

    public void setId(Long id){
        this.id = id;
    }

    public Long getId(){
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object object){
        if(object instanceof Concert){
            Concert otherConcert = (Concert)object;
            // Assuming we won't get any ID clashes
            if(otherConcert.getId().equals(this.id) && otherConcert.getTitle().equals(this.title)){
                return true;
            }
        }
        return false;
    }
}
