package se325.lab01.concert.common;

/**
 * Class with configuration settings for the application.
 */
public class Config {

    public static final int REGISTRY_PORT = 8090;

    // Name used to advertise/register the ConcertService in the RMI Registry.
    public static final String SERVICE_NAME = "concert-service";
}

