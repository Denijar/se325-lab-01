package se325.lab01.concert.server;

import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public static void main(String[] args) {
        try {

            // Create the registry on the local host
            Registry lookupService = LocateRegistry.createRegistry(Config.REGISTRY_PORT);

            // Instantiate the se325.lab01.concert.server.ConcertServiceServant
            ConcertService service = new ConcertServiceServant();

            // Advertise the ConcertService using the registry
            lookupService.rebind(Config.SERVICE_NAME, service);

            System.out.println("Server hosted on port: " + Config.REGISTRY_PORT);
            Keyboard.prompt("Press Enter to shutdown the server");
            lookupService.unbind(Config.SERVICE_NAME);

            System.out.println("The Concert Service is no longer bound in the RMI registry. Waiting for lease to expire.");

        } catch (RemoteException e){
            System.err.println("Unable to start or register proxy with the RMI Registry");
            e.printStackTrace();
        } catch (NotBoundException e){
            System.err.println("Unable to remove proxy from the RMI Registry");
        }
    }
}
