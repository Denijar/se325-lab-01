package se325.lab01.concert.server;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class ConcertServiceServant extends UnicastRemoteObject implements ConcertService {

    private final Map<Long, Concert> concerts = new HashMap<>();

    public ConcertServiceServant() throws RemoteException {
        super();
    }

    public synchronized Concert createConcert(Concert concert) throws RemoteException {
        Long id = idGenerator();
        concert.setId(id);
        concerts.put(id, concert);
        return concert;
    }

    public synchronized Concert getConcert(Long id) throws RemoteException {
        return concerts.get(id);
    }

    public synchronized boolean updateConcert(Concert concert) throws RemoteException {
        if(concerts.containsKey(concert.getId())){
            concerts.put(concert.getId(), concert);
            return true;
        } else {
            return false;
        }
    }

    public synchronized boolean deleteConcert(Long id) throws RemoteException {
        if(concerts.containsKey(id)){
            concerts.remove(id);
            return true;
        } else {
            return false;
        }
    }

    public synchronized List<Concert> getAllConcerts() throws RemoteException {
        return new ArrayList<>(concerts.values());
    }

    public synchronized void clear() throws RemoteException {
        concerts.clear();
    }

    /**
     * Helper method to generate unique Long ids
     * @return an ID of type Long
     */
    private Long idGenerator(){
        return UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE;
    }
}
