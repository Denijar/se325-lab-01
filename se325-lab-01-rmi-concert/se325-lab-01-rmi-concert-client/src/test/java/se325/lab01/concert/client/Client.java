package se325.lab01.concert.client;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.common.Config;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.fail;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;


/**
 * JUnit test client for the RMI Concert application
 */
public class Client {

    // Proxy object to represent the remote ConcertService object
    private static ConcertService proxy;

    /**
     * One-time setup method to retrieve the ConcertService proxy from the RMI registry
     */
    @BeforeClass
    public static void getProxy(){
        try{
            // instantiate a proxy object for the local registry
            Registry lookupService = LocateRegistry.getRegistry("localhost", Config.REGISTRY_PORT);

            // Retrieve a proxy object to represent the remote ConcertService
            proxy = (ConcertService) lookupService.lookup(Config.SERVICE_NAME);
        } catch (RemoteException e) {
            System.err.println("Unable to connect to the RMI Registry");
            e.printStackTrace();
        } catch (NotBoundException e) {
            System.err.println("Unable to acquire a proxy for the Concert Service");
            e.printStackTrace();
        }

    }

    @Before
    public void clearConcertList(){
        try {
            proxy.clear();
        } catch (RemoteException e){
            System.err.println("Unable to connect to the RMI server");
            e.printStackTrace();
        }
    }

    /**
     * Tests the remote invocation of the create method on the ConcertService
     */
    @Test
    public void testCreate() throws RemoteException{
        try{
            Concert concertA = new Concert("The Killers", LocalDateTime.now());
            Concert concertAWithId = proxy.createConcert(concertA);
            Concert concertB = new Concert("Arctic Monkeys", LocalDateTime.now());
            Concert concertBWithId = proxy.createConcert(concertB);

            // Query the remote factory
            List<Concert> concerts = proxy.getAllConcerts();
            assertTrue(concerts.contains(concertAWithId));
            assertTrue(concerts.contains(concertBWithId));
            assertEquals(concerts.size(), 2);

        } catch (Exception e){
            fail();
        }
    }

    @Test
    public void testUpdate() throws RemoteException{
        try{
            Concert concertA = new Concert("The Killers", LocalDateTime.now());
            Concert concertAWithId = proxy.createConcert(concertA);
            concertAWithId.setTitle("Arctic Monkeys'");
            proxy.updateConcert(concertAWithId);

            // Query the remote factory
            List<Concert> concerts = proxy.getAllConcerts();
            assertTrue(concerts.contains(concertAWithId));
            assertEquals(concerts.size(), 1);

        } catch (Exception e){
            fail();
        }
    }

}
